# robot-blocks-problem

## Usage

`yarn run sample` to run with the sample script provided in the PDF.

`node robot.js <input_file>` to run with a custom input file.