/**
 * @todo try to combine moveOnto + moveOver and pileOnto + pileOver
 */

const fs = require('fs');

function readInput() {
    let input = fs.readFileSync(process.argv[2], 'utf8').split('\n');
    // console.log(input);
    return input;
}

function getIndexes({blocks, source, target}) {
    let indexTarget1, // first dimension
        indexTarget2, // second dimension
        indexSource1, // first dimension
        indexSource2; // second dimension

    for (let i = 0; i < blocks.length; i++) {
        let indexTarget = blocks[i].indexOf(target);
        let indexSource = blocks[i].indexOf(source);
        if (indexTarget > -1) {
            indexTarget1 = i;
            indexTarget2 = indexTarget;
        }
        if (indexSource > -1) {
            indexSource1 = i;
            indexSource2 = indexSource;
        }
    }

    return {
        indexTarget1,
        indexTarget2,
        indexSource1,
        indexSource2
    };
}

function unstack({blocks, index1, index2}) {
    return blocks[index1].splice(index2 + 1);
}

function reposition({blocks, blocksToReposition}) {
    for (let i = 0; i < blocksToReposition.length; i++) {
        let blockToMove = blocksToReposition[i];
        blocks[blockToMove] = [blockToMove];
    }    
}

function unstackAndReposition({blocks, index1, index2}) {
    let blocksToReposition = unstack({
        blocks,
        index1,
        index2
    });

    reposition({
        blocks,
        blocksToReposition
    });
}

function move({blocks, source, target, how}) {
    const {
        indexTarget1,
        indexTarget2,
        indexSource1,
        indexSource2
    } = getIndexes({
        blocks,
        source,
        target
    });

    unstackAndReposition({
        blocks,
        index1: indexSource1,
        index2: indexSource2
    });

    if (how === 'onto') {
        unstackAndReposition({
            blocks,
            index1: indexTarget1,
            index2: indexTarget2
        });
    }

    blocks[indexTarget1] = [...blocks[indexTarget1], source];
    blocks[source] = [];
}


function pile({blocks, source, target, how}) {
    const {
        indexTarget1,
        indexTarget2,
        indexSource1,
        indexSource2
    } = getIndexes({
        blocks,
        source,
        target
    });

    if (how === 'onto') {
        unstackAndReposition({
            blocks,
            index1: indexTarget1,
            index2: indexTarget2,
        });
    }

    let stackToMove = blocks[indexSource1].slice(indexSource2);
    blocks[indexTarget1] = blocks[indexTarget1].concat(stackToMove);
    blocks[indexSource1].splice(indexSource2, stackToMove.length);
}

function output({blocks}) {
    blocks.forEach((stack, index) => {
        let str = index + ': ';
        stack.forEach(block => {
            str += block + ' ';
        });
        console.log(str);
    });
}

function getBlocks(input) {
    let blocks = [];
    let blockCount = input[0];

    for (let i = 0; i < blockCount; i++) {
        blocks[i] = [i];
    }
    return blocks;
}

function main() {
    const input = readInput();
    const blocks = getBlocks(input);

    for (let i = 1; i < input.length; i++) {
        const command = input[i].split(' ');
        if (command[0] === 'quit') {
            break;
        }
        const source = parseInt(command[1]);
        const target = parseInt(command[3]);
        const how = command[2];

        if (source === target) {
            continue;
        }
        if (blocks[source].indexOf(target) > -1) {
            continue;
        }
        if (blocks[target].indexOf(source) > -1) {
            continue;
        }

        switch (command[0]) {
            case 'move':
                move({
                    blocks, 
                    source, 
                    target, 
                    how
                });
                break;
            case 'pile':
                pile({
                    blocks, 
                    source, 
                    target, 
                    how
                });
                break;
            default:
        }
        // output();
    }

    output({blocks});
}

main();

